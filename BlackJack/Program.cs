﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJack
{
    // Algorithm for blackjack game imitation:
    // Every game starts with the creation of 52-card deck that is stored as an array of integers
    // Every card is encoded as a sum of Suit code and Face code, where
    // 100 is Heart  200 is Diamond 300 is Spade 400 is Club
    // 2 - 10 are coded as is, Jack is 11, Queen 12, King 13 and finally Ace is 14
    // So for example Quenn of Hearts is 300 + 12 = 312 in an array
    // When card is selected it is marked as 0 in array so that we dont pick it next time
    // Card selection is done by picking random value from 0 to 51 and checking if card was not used before
    // Game starts by selecting 2 cards from the deck randomly and summing up total value
    // the player is asked whether he wants to twist or stick
    //If player asks 'Twist' Dealer takes card for player. If player scores more then 21, then he loses
    //If player asks 'Stick', Dealer takes cards until his score is less than 17 or more then 21
    //If Dealer has more then 21, then player wins
    //If Player and Dealer still in the game, program compare they scores. Biggest score wins
    //After that player can play again or exit

    class BlackJack_Program
    {
        // we store suits as hundreds in an integer
        // to find out suite we divide card value by 100 and multiply by 100 again

        public enum Suit
        {
            Heart = 100,
            Diamond = 200,
            Spade = 300,
            Club = 400
        }

        public BlackJack_Program()
        {
            CreateDeck();
        }

        public enum Face
        {
            Jack = 11,
            Queen = 12,
            King = 13,
            Ace = 14
        }

        private int m_PlayerScore = 0, m_DealerScore = 0; // scores of both players in current game
        int m_CurrentCardPosition;
        int[] m_Deck = new int[52];
        Random m_CardRandomizer = new Random();
        string m_StickOrTwist;

        // Filling the Deck for one game.
        // we encode all cards as integers in array, hundreds are used for suits
        // 2-10 are used 'as - is' , Jack  - Ace has special values
        private void CreateDeck() 
        {
            int CardNumber = -1;
            var valuesOfSuits = Enum.GetValues(typeof(Suit)); //Suits integer array from enum "Suit"
            foreach (Suit suit in valuesOfSuits)
            {
                for (int i = 2; i <= 14; ++i) //all 13 Cards of the same suit;
                {
                    CardNumber++;
                    m_Deck[CardNumber] = i + (int)suit;
                }
            }
        }

        public int GetCard() // pick card from the deck randomly
        {
            string CardName;
            int CardValue;
            // try random position in the deck until we get non-marked card
            do
            {
                m_CurrentCardPosition = m_CardRandomizer.Next(52);
            }
            while (m_Deck[m_CurrentCardPosition] == 0);

            GetNameAndValue(out CardName, out CardValue);//Read Name and value of Card
            Console.WriteLine("Card dealt is the {0} of {1}s, value {2}", CardName, GetSuit(), CardValue);
            m_Deck[m_CurrentCardPosition] = 0; // mark card as removed (we can't remove values from array)
            return CardValue;
        }

        // Extract name and value of current card
        // for cards below Jack name and value is the same numerical value stored in the deck
        // for other cards we get name from Face enum and values are 10 for Jack,King, Queen and Ace is 11
        private void GetNameAndValue(out string CardName, out int CardValue)
        {
            int CardNameID = m_Deck[m_CurrentCardPosition] % 100; //We throw away hundreds to throw away suits
            if (CardNameID < 11)
            {
                CardName = CardNameID.ToString();
                CardValue = CardNameID;
            }
            else
            {
                Face face = (Face)CardNameID;
                CardName = face.ToString();
                if (CardNameID == 14)
                    CardValue = 11;
                else
                    CardValue = 10;
            }
        }

        private string GetSuit() //Extract suit of current card
        {
            int Index = m_Deck[m_CurrentCardPosition] / 100;
            Index *= 100;
            Suit CardSuit = (Suit)Index;
            return CardSuit.ToString();
        }

        //The Gameplay is centered in the method Play
        void Play()
        {
            m_PlayerScore += GetCard(); //Card for Player, add value to Player score
            m_PlayerScore += GetCard();
            Console.WriteLine("Player score is {0}", m_PlayerScore + "\n Stick or twist – s/t");
            m_StickOrTwist = Console.ReadLine();//What's player answered
            while (true) //Repeat question until player asks for Twist or Lose;
            {
                if (m_StickOrTwist == "s")  
                {
                    m_DealerScore += GetCard();
                    m_DealerScore += GetCard();
                    Console.WriteLine("Dealer score is {0}", m_DealerScore);
                    Stick();
                    break;
                }
                else if (m_StickOrTwist == "t")
                {
                    m_PlayerScore += GetCard();
                    if (!Twist())
                        break;
                }
                else
                {
                    Console.WriteLine("Enter 's' or 't'");
                    m_StickOrTwist = Console.ReadLine();
                }
            }
            m_DealerScore = 0;
            m_PlayerScore = 0;
        }

        private bool Twist() //If player asks 'Twist' Dealer takes card for player. If player scores more then 21, then he loses 
        {
            if (m_PlayerScore > 21)
            {
                Console.WriteLine("Player score is {0}", m_PlayerScore + "\n Player went bust, Diller wins");
                return false;
            }
            else
            {
                Console.WriteLine("Player score is {0}", m_PlayerScore + "\n Stick or twist – s/t");
                m_StickOrTwist = Console.ReadLine();
            }
            return true;
        }

        private void Stick() //If player asks 'Stick', Dealer takes cards until his score is less than 17 or more then 21
        {
            
            while (m_DealerScore <= 17)
            {
                m_DealerScore += GetCard();
                Console.WriteLine("Dealer score is {0}", m_DealerScore);
            }
            if (m_DealerScore > 21) //If Dealer has more than 21 then he loses
            {
                Console.WriteLine("Dealer score is more than 21 \n Congratulations! You win!");
                return;
            }
            else //If dealer and player are still playing their scores compared
            {
                if (m_PlayerScore > m_DealerScore)
                    Console.WriteLine("Congratulations! You win!");
                else if (m_PlayerScore < m_DealerScore)
                    Console.WriteLine("Dealer wins!");
                else
                {
                    Console.WriteLine("Draw!");
                }
                return;
            }
        }

        static void Main(string[] args)
        {
            BlackJack_Program Game = new BlackJack_Program();
            Console.WriteLine("Welcome to biggest BlackJack room!");
            Game.Play();
            while (true) //repeat as long as player wants to Play
            {
                Console.WriteLine("Would you like to play again – yes/no?");
                string Answer = Console.ReadLine();
                if (Answer == "yes")
                {
                    Game.Play();
                }
                else if (Answer == "no")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Incorect answer, enter 'yes' or 'no'");
                }
            }
        }
    }
}
